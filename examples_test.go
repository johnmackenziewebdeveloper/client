package client_test

import (
	"context"
	"fmt"
	"time"

	"encoding/json"

	"net/http"

	"bitbucket.org/johnmackenziewebdeveloper/client"
)

func ExampleGetArtists() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	cli := http.Client{
		Timeout: 1 * time.Second,
	}

	artistCli, err := client.New("http://0.0.0.0:8080", client.WithHTTPClient(&cli))
	checkError(err)

	out, err := artistCli.Artists.Get(ctx)
	checkError(err)

	b, err := json.MarshalIndent(out, "", "  ")
	checkError(err)

	fmt.Println(string(b))
	// Output: [
	//  {
	//    "name": "Morrissey",
	//    "uuid": "f2173162-c132-4b65-9b00-89c9d1033d8d"
	//  }
	//]

}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

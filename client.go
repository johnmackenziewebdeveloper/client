package client

type APIClient struct {
	Artists  ArtistsAPI
	Releases ReleaseAPI
	Genres   GenreAPI
}

func New(endpoint string, opts ...ClientOption) (*APIClient, error) {
	genClient, err := NewClient(endpoint, opts...)

	if err != nil {
		return nil, err
	}
	return &APIClient{
		Artists: ArtistsAPI{
			client: genClient,
		},
		Releases: ReleaseAPI{
			client: genClient,
		},
		Genres: GenreAPI{
			client: genClient,
		},
	}, nil
}

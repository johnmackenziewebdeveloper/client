package client

import "errors"

// ErrArtistAlreadyExists used for when artist already exists on PostArtists endpoint
var ErrArtistAlreadyExists = errors.New("an artist with those properties already exists")

// ErrInternalServerError for when an unknown errors occurs in downsteam api
var ErrInternalServerError = errors.New("internal server error received from downstream api")

// ErrFetchingAllArtists a common error received when attempting to fetch all artists
var ErrFetchingAllArtists = errors.New("error fetching all artists")

var ErrFetchingArtistsReleases = errors.New("error fetching artists releases")

var ErrUnknown = errors.New("an unknown error occurred")

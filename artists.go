package client

import (
	"context"
	"net/http"
)

// artistAPI holds the available methods for artists
type artistAPI interface {
	Get(ctx context.Context) ([]Artist, error)
	PostArtists(ctx context.Context, body PostArtistsJSONBody) (*Artist, error)
	AttachGenre(ctx context.Context, artistID, genreID string) error
}

// _ compile time check to ensure that ArtistsAPI exported type fulfills the above interface
var _ artistAPI = (*ArtistsAPI)(nil)

//go:generate mockery -dir=./generated -output=./mocks -outpkg=mocks -name=ClientInterface
// ArtistAPI handles all artist endpoints
type ArtistsAPI struct {
	client ClientInterface
}

func (a ArtistsAPI) AttachGenre(ctx context.Context, artistID, genreID string) error {
	_, err := a.client.PostArtistsIdGenres(ctx, artistID, PostArtistsIdGenresJSONRequestBody{
		Uuid: genreID,
	})
	return err
}

func (a ArtistsAPI) GetArtistsGenres(ctx context.Context, artistID, genreID string) ([]Genre, error) {
	res, err := a.client.GetArtistsIdGenres(ctx, artistID)
	if err != nil {
		return nil, err
	}

	out := GenreList{}
	err = unmarshalResponseBody(res, &out)
	if err != nil {
		return nil, err
	}
	return out.Data, nil
}

func (a ArtistsAPI) GetArtistsIdReleases(ctx context.Context, artistID string) ([]Release, error) {
	r, err := a.client.GetArtistsIdReleases(ctx, artistID)

	if err != nil {
		return nil, err
	}

	if r.StatusCode != http.StatusOK {
		return nil, ErrFetchingArtistsReleases
	}

	out := ReleaseList{}
	err = unmarshalResponseBody(r, &out)
	if err != nil {
		return nil, err
	}
	releases := []Release{}

	for _, a := range out.Data {
		releases = append(releases, Release(a))
	}
	return releases, nil
}

func (a ArtistsAPI) GetArtistByID(ctx context.Context, id string) (*Artist, error) {
	r, err := a.client.GetArtistsId(ctx, id)

	if err != nil {
		return nil, err
	}

	in := SingleArtist{}

	err = unmarshalResponseBody(r, &in)

	if err != nil {
		return nil, err
	}

	return &in.Data, nil
}

func (a ArtistsAPI) Get(ctx context.Context) ([]Artist, error) {
	r, err := a.client.GetArtists(ctx)

	if err != nil {
		return nil, err
	}

	if r.StatusCode != http.StatusOK {
		return nil, ErrFetchingAllArtists
	}

	out := ArtistList{}
	if err := unmarshalResponseBody(r, &out); err != nil {
		return nil, err
	}

	artists := []Artist{}

	for _, a := range out.Data {
		artists = append(artists, Artist(a))
	}
	return artists, nil
}

func (a ArtistsAPI) PostArtists(ctx context.Context, body PostArtistsJSONBody) (*Artist, error) {
	r, err := a.client.PostArtists(ctx, PostArtistsJSONRequestBody{
		Name: body.Name,
	})
	if err != nil {
		return nil, err
	}

	switch r.StatusCode {
	case http.StatusBadRequest:
		return nil, ErrArtistAlreadyExists
	case http.StatusInternalServerError:
		return nil, ErrInternalServerError
	case http.StatusCreated:
		// this is what we want, pass thought and continue processing
		break
	default:
		return nil, ErrUnknown
	}

	out := Artist{}
	if err := unmarshalResponseBody(r, &out); err != nil {
		return nil, err
	}

	return &out, nil
}

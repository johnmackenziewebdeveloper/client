package client

import "context"

// genreAPI holds the available methods for artists
type genreAPI interface {
	GetGenres(ctx context.Context) ([]Genre, error)
}

// _ compile time check to ensure that GenreAPI exported type fulfills the above interface
var _ genreAPI = (*GenreAPI)(nil)

//go:generate mockery -dir=./generated -output=./mocks -outpkg=mocks -name=ClientInterface
// genreAPI handles all artist endpoints
type GenreAPI struct {
	client ClientInterface
}

func (g GenreAPI) GetGenres(ctx context.Context) ([]Genre, error) {
	res, err := g.client.GetGenres(ctx)
	if err != nil {
		return nil, err
	}

	out := GenreList{}
	if err := unmarshalResponseBody(res, &out); err != nil {
		return nil, err
	}

	return out.Data, nil
}

func (g GenreAPI) PutGenres(ctx context.Context, name string) (*Genre, error) {
	res, err := g.client.PutGenres(ctx, PutGenresJSONRequestBody{
		Name: name,
	})
	if err != nil {
		return nil, err
	}

	out := SingleGenre{}
	if err := unmarshalResponseBody(res, &out); err != nil {
		return nil, err
	}

	return &out.Data, nil
}

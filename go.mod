module bitbucket.org/johnmackenziewebdeveloper/client

go 1.14

require (
	github.com/deepmap/oapi-codegen v1.3.7
	github.com/getkin/kin-openapi v0.8.0
	github.com/labstack/echo/v4 v4.1.16
)

package client

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// releaseAPI holds the available methods for artists
type releaseAPI interface {
	GetReleases(ctx context.Context, params GetReleasesParams) ([]Release, error)
	PostRelease(ctx context.Context, body PostReleasesJSONRequestBody) (*Release, error)
}

// _ compile time check to ensure that ReleaseAPI exported type fulfills the above interface
var _ releaseAPI = (*ReleaseAPI)(nil)

//go:generate mockery -dir=./generated -output=./mocks -outpkg=mocks -name=ClientInterface
// releaseAPI handles all artist endpoints
type ReleaseAPI struct {
	client ClientInterface
}

func (r ReleaseAPI) GetReleases(ctx context.Context, params GetReleasesParams) ([]Release, error) {
	res, err := r.client.GetReleases(ctx, &GetReleasesParams{
		ReleaseDate: params.ReleaseDate,
		Limit:       params.Limit,
		Offset:      params.Offset,
	})

	if err != nil {
		return nil, err
	}
	out := ReleaseList{}
	if err := unmarshalResponseBody(res, &out); err != nil {
		return nil, err
	}

	return out.Data, nil
}

func (r ReleaseAPI) PostRelease(ctx context.Context, body PostReleasesJSONRequestBody) (*Release, error) {
	res, err := r.client.PostReleases(ctx, body)
	if err != nil {
		return nil, err
	}

	out := Release{}
	if err := unmarshalResponseBody(res, &out); err != nil {
		return nil, err
	}

	rtn := Release(out)
	return &rtn, nil
}

func (r ReleaseAPI) PutRelease(ctx context.Context, id string, body PutReleasesIdJSONRequestBody) (*Release, error) {
	res, err := r.client.PutReleasesId(ctx, id, body)
	if err != nil {
		return nil, err
	}

	out := Release{}
	if err := unmarshalResponseBody(res, &out); err != nil {
		return nil, err
	}

	return &out, nil
}

func unmarshalResponseBody(r *http.Response, v interface{}) error {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	err = r.Body.Close()

	if err != nil {
		return err
	}

	return json.Unmarshal(b, v)
}
